package com.geopagos.ejercicio1.Factory;

import com.geopagos.ejercicio1.ErrorHandler.GenericExceptions;
import com.geopagos.ejercicio1.ProductsFactory.*;


public class MethodFactory {
    IFigura figura;

    public IFigura producirFigura(String tipoFigura, Double base, Double altura, Double diametro) throws Exception {

        switch (tipoFigura.toUpperCase()){
            case "CIRCULO":
                figura = new Circulo(diametro);
                break;
            case "CUADRADO":
                figura = new Cuadrado(base);
                break;
            case "TRIANGULO":
                figura = new Triangulo(base, altura);
                break;
            default:
                throw new GenericExceptions("Tipo de figura desconocido: " + tipoFigura.toUpperCase());
        }
    return figura;
    }
}
