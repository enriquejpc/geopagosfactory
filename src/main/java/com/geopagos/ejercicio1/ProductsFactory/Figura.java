package com.geopagos.ejercicio1.ProductsFactory;

import lombok.*;


@AllArgsConstructor
@ToString
public abstract class Figura {
    String tipoFigura;
    Double base;
    Double altura;
    Double diametro;

}
