package com.geopagos.ejercicio1.ProductsFactory;

public class Circulo extends Figura implements IFigura {
    public Circulo( Double diametro) {
        super("CIRCULO", null, null, diametro);
    }


    @Override
    public String getTipoFigura() {
        return "CIRCULO";
    }

    @Override
    public Double getSuperficie() {
        return Double.valueOf((Math.round((Math.PI)*Math.pow((this.diametro/2),2.0))));
    }

    @Override
    public Double getBase() {
        return null;
    }

    @Override
    public Double getAltura() {
        return null;
    }

    @Override
    public Double getDiametro() {
        return this.diametro;
    }
}
