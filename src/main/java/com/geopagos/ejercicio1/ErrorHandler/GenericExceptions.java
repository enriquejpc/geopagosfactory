package com.geopagos.ejercicio1.ErrorHandler;

public class GenericExceptions extends Exception {

    public GenericExceptions(String mensaje){
        super(mensaje);
    }
}
