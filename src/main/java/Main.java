
import com.geopagos.ejercicio1.ErrorHandler.GenericExceptions;
import com.geopagos.ejercicio1.Factory.MethodFactory;
import com.geopagos.ejercicio1.ProductsFactory.Figura;
import com.geopagos.ejercicio1.ProductsFactory.IFigura;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws Exception, GenericExceptions {
        MethodFactory factory = new MethodFactory();

        ArrayList<IFigura> figuras = new ArrayList<>();

        figuras.add( factory.producirFigura("circulo",null,null,18d));
        figuras.add( factory.producirFigura("triangulo",6.00,4.00,null));
        figuras.add( factory.producirFigura("cuadrado",12.00,null,null));

        figuras.forEach(figura->System.out.println(
                "Figura: "+figura.getTipoFigura()+
                " /Atributos -> Base: "+figura.getBase()+
                " Altura: " +figura.getAltura() +
                " Diametro: "+ figura.getDiametro()+
                " Superficie: "+ figura.getSuperficie()
        ));

    }
}
